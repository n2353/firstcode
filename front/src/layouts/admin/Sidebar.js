import React from 'react';
import { Link } from 'react-router-dom';
class Sidebar extends React.Component {


    showP=()=>{

        document.getElementById('showP').style.display==='none'?document.getElementById('showP').style.display='block':document.getElementById('showP').style.display='none';

    }
    render() { 
        return (  
            <div className="col-2" style={{ padding:0 }}>
                <div className="d-flex flex-column flex-shrink-0 p-3 text-white bg-dark" style={{height:1000 }}>
                    <Link to="/admin" className="d-flex    text-white text-decoration-none float-end">
                    <svg className="bi me-2" width="40" height="32"></svg>
                    <span className="fs-4">منوی مدیریت</span>
                    </Link>
                    <hr/>
                    <ul className="nav nav-pills flex-column mb-auto">
                    <li className="nav-item">
                        <Link to="/admin" style={{ fontSize:'25px' }} className="nav-link active" aria-current="page">
                        <svg className="bi me-2"  width="16" height="16"></svg>
                                                داشبورد

                        </Link>
                    </li>
                    
                    
                    <li>
                        <Link to="#" style={{ fontSize:'25px',paddingBottom:0 }} onClick={this.showP} className="nav-link text-white">
                        <svg className="bi me-2" width="16" height="16"></svg>
                        ‍‍‍بست ها :    
                        
                        </Link>
                        <div className="col-12"  style={{ padding:0}}>
                            <div className="d-flex flex-column flex-shrink-0 text-white bg-dark"  >
                                                <ul style={{display:'none',padding:0 }} id='showP' className="nav nav-pills flex-column mb-auto">
                                                     <li>
                                                    <Link to="/admin/create-post" style={{ fontSize:'22px' }} className="nav-link text-white">
                                                            <svg className="bi me-2" width="16" height="16"></svg>

                                                            اضافه کردن 
                                                    </Link>
                                                    </li>
                                                    <li>
                                                    <Link to="/admin/posts" style={{ fontSize:'22px' }} className="nav-link text-white">
                                                            <svg className="bi me-2" width="16" height="16"></svg>

                                                            مشاهده بست ها
                                                    </Link>
                                                    </li>
                                                    
                                                </ul>

                            </div>
                        </div>
                    </li>

                    
                    

                      <li>
                        <Link to="/admin/about-us" style={{ fontSize:'25px',paddingBottom:0 }} className="nav-link text-white">
                        <svg className="bi me-2" width="16" height="16"></svg>
                       درباره ما    
                        
                        </Link>
                        </li>


                    <li>
                        <Link to="#" className="nav-link text-white">
                        <svg className="bi me-2" width="16" height="16"></svg>
                        Customers
                        </Link>
                    </li>


                    
                    </ul>
                    <hr/>
                    <div className="dropdown">
                    <Link to="#" className="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="https://github.com/mdo.png" alt="" width="32" height="32" className="rounded-circle me-2"/>
                        <strong>mdo</strong>
                    </Link>
                    <ul className="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
                        <li><Link className="dropdown-item" to="#">New project...</Link></li>
                        <li><Link className="dropdown-item" to="#">Settings</Link></li>
                        <li><Link className="dropdown-item" to="#">Profile</Link></li>
                        <li><hr className="dropdown-divider"/></li>
                        <li><Link className="dropdown-item" to="#">Sign out</Link></li>
                    </ul>
                    </div>
                </div>
            </div>      


               
        );
    }
}
 
export default Sidebar;