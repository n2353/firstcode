import React from 'react';
import Sidebar from './Sidebar';
import { Switch, Route } from 'react-router-dom';
import adminRouts from '../../routs/AdminRouts';
class Master extends React.Component {
    render() { 
        return (
            <div dir='rtl' className="row" >
            <Sidebar/>
            <div  style={{ padding:0 }}  className="col-10">
                <nav className="navbar navbar-dark bg-dark" style={{ textAlign:'center',paddingRight:'42%' }}>
                        <span className="fs-4 navbar-brand text-center "> ‍‍‍بخش مدیریت سایت
                        </span>

                    
                </nav>
                <Switch>
                        {
                            adminRouts.map(route=>{
                                return (
                                    <Route key={route.id} exact={route.exact} name={route.name} path={route.path} render={(props)=>{
                                    return (
                                        <route.component {...props} {...this.props} />
                                    )
                                }} />
                                )
                            })
                        }
                    </Switch>
                </div> 
  
            </div>
        );
    }
}
 
export default Master;