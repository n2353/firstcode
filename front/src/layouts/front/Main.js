import React from 'react';
import Navbar from './Navbar';
import { Switch, Route } from 'react-router-dom';
import publicRouts from '../../routs/PublicRouts';
class Main extends React.Component {
    render() { 

        
        return (
            <div>
                <Navbar {...this.props} />
                <Switch>
                    {
                        publicRouts.map(route=>{
                            return (
                                <Route key={route.id} exact={route.exact} name={route.name} path={route.path} render={(props)=>{
                                return (
                                    <route.component {...props} {...this.props} />
                                )
                            }} />
                            )
                        })
                    }
                </Switch>                
            </div>);
    }
}
 
export default Main;