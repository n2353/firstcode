import axios from 'axios';
import React from 'react';
import { Link } from 'react-router-dom';
import { changeToken } from '../../actions';
import { connect } from 'react-redux';
class Navbar extends React.Component {


    componentDidMount=()=>{
         this.props.dispatch(changeToken(localStorage.getItem('auth_token')?localStorage.getItem('auth_token'):''));
    }
    

    logOut=(e)=>{       
        axios.get('sanctum/csrf-cookie').then(response => {
            axios.get(axios.defaults.baseURL+'/api/logout').then(res=>{
                try {
                    if(res.data.status===200){
                        localStorage.removeItem('auth_token');
                        localStorage.removeItem('auth_name');
                        this.props.dispatch(changeToken(localStorage.getItem('auth_token')?localStorage.getItem('auth_token'):''));

                        
                    
                    }
                    else{ 


                    }
                } catch (error) {
                    console.log(error);
                }
            });
        });
        
    }
    
    render() { 
        return (
            <div >
               <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                <div className="navbar-collapse collapse w-100 order-1 order-md-0 dual-collapse2">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link className="nav-link" to="#">Left</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="//codeply.com">Codeply</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="#">Link</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="#">Link</Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="#">Link</Link>
                        </li>
                    </ul>
                </div>
                
                <div style={{ paddingLeft:'35%' }}  className="navbar-collapse collapse order-3 w-50 text-end  dual-collapse2">
                    {this.props.auth_token!==''?(
                        <ul className="navbar-nav ml-auto">
                        
                        <li className="nav-item">
      
                            <Link className="nav-link" to="/" onClick={this.logOut}><h5>خروج</h5></Link>
                        </li>
                    </ul>
                    ):(
                        <ul className="navbar-nav ml-auto">
                        
                        <li className="nav-item">
                            
                            <Link className="nav-link" to="/login"><h5>ورود</h5></Link>
                        </li>
                        <li className="nav-item">
                            <Link className="nav-link" to="/register"><h5>ثبت نام</h5></Link>
                        </li>
                    </ul>
                    )}
                    
                </div>
            </nav>
            </div>);
    }
}
 
export default connect((state)=>{return state}) (Navbar);
