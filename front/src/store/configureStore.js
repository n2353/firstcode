import {
    createStore
} from 'redux';

import mainReducer from '../reducers/mainReducer';

export default (defaultState)=>{
    const store=createStore(mainReducer,defaultState);
    return store; 
};