
const mainReducer=(state=[],action)=>{
          switch (action.type) {
            case 'INCREMENT_VALUE':
                  const value=state.val;
                  return {val:value+1};
            case 'HANDLE_INPUT':
                  const e=action.e;
                  return {...state,[e.target.name]:e.target.value};
            case 'CHANGE_INPUT':
                  return {...state,[action.input]:action.value};
            case 'CLEAR_STATE':
                  return {...state};
            case 'HANDLE_FILE':
                  const ee=action.e;
                  return {...state,[ee.target.name]:ee.target.files[0]};
             case 'HANDLE_ERRORS':
                  const errors=action.errors;
                  return {...state,errors:errors};
            case 'CHANGE_TOKEN':
            const token=action.token;
            return {...state,auth_token:token};
            default:
                return state;

          }

    
    
}

export default mainReducer;
