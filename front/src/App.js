import React from 'react';
import {connect} from 'react-redux';
import { BrowserRouter,Route,Switch,Redirect } from 'react-router-dom';
import axios from 'axios';
import AdminRouter from './routers/AdminRouter';
import PublicRouter from './routers/PublicRouter';
import Register from './components/auth/Register';
import Login from './components/auth/Login';

axios.defaults.baseURL='http://127.0.0.1:8000';
axios.defaults.headers.post["Accept"]="application/json";
axios.defaults.headers.post["Content-Type"]="application/json";
axios.defaults.withCredentials = true;

axios.interceptors.request.use(function (config) {
  const token=localStorage.getItem('auth_token')?'Bearer '+localStorage.getItem('auth_token'):'';
  config.headers.Authorization=token;
  return config;

  
});



class App extends React.Component {

  

  render() { 
   
    return (
    <div className="App">

      <BrowserRouter>
        <Switch> 
          <Route path='/admin' name="admin" render={ (props)=>{
            
          return (  <AdminRouter {...props}  />)
            
          }} />

          <Route path='/register' name="register" render={ (props)=>{
            
          return localStorage.getItem('auth_token')?(<Redirect to="/" />):(<Register {...props}  />)
            
          }} />

          
           <Route path='/login' name="login" render={ (props)=>{
            
          return localStorage.getItem('auth_token')?(<Redirect to="/" />):(<Login {...props}  />)
            
          }} />


          <Route path="/"  render={ (props)=>{
            
          return (  <PublicRouter {...props} />)
            
          }} />

          <Redirect to="/" />
        </Switch>
      </BrowserRouter>

      
    </div>
  );
  }
}
 

export default connect((state)=>{return state}) (App);
