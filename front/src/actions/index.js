export const incrementValue =(Value)=>{

    return {
        type:'INCREMENT_VALUE',
        Value:Value
    }

}

export const handleInput=(e)=>{

    return {
        type:"HANDLE_INPUT",
        e:e
    }
}
export const clearState=()=>{

    return {
        type:"CLEAR_STATE",
    }
}
export const changeInputs=(input,value)=>{

    return {
        type:"CHANGE_INPUT",
        input:input,
        value:value
    }
}


export const handleErrors=(errors)=>{

    return {
        type:"HANDLE_ERRORS",
        errors:errors
    }
}


export const changeToken=(token)=>{

    return {
        type:"CHANGE_TOKEN",
        token:token
    }
}
export const handleFile=(e)=>{

    return {
        type:"HANDLE_FILE",
        e:e
    }
}
