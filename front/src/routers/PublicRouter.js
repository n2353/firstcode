import axios from 'axios';
import React from 'react';
import swal from 'sweetalert';
import Main from '../layouts/front/Main';

class PublicRouter extends React.Component {
    
    componentDidMount(){
        // axios.get('sanctum/csrf-cookie').then(response => {
        //     axios.get('/api/checkingAuthenticated').then(res=>{
        //         if(res){
        //             this.setState({...this.state,statusCode:res.status})

        //         }
        //     })


        // });
        
         axios.interceptors.response.use((response)=>{
            return response
        },(error)=>{
            if (error.response.status===320) {
                swal('Error','شما اجازه دسترسی به این صفحه را ندارید','error');
                this.props.history.push('/');
                
            }else if (error.response.status===401) {
                swal('Error','شما احراز هویت نشده اید','error');
                this.props.history.push('/');

            }else if (error.response.status===300) {
                 swal('Error','شما احراز هویت نشده اید','error');
                this.props.history.push('/');

                
            }

        })

    
    }
    render() { 
        return <Main {...this.props} />;
    }
}
 
export default PublicRouter;