import axios from 'axios';
import React from 'react';
import swal from 'sweetalert';
import Master from '../layouts/admin/Master';

class AdminRouter extends React.Component {
    
    componentDidMount(){
        axios.get('sanctum/csrf-cookie').then(response => {
            axios.get('/api/checkingAuthenticated').then(res=>{
                if(res){
                    console.log(res.data);
                }
            })


        });

        axios.interceptors.response.use((response)=>{
            return response
        },(error)=>{
            if (error.response.status===320) {
                swal('Error','شما اجازه دسترسی به این صفحه را ندارید','error');
                this.props.history.push('/');
                
            }else if (error.response.status===401) {
                swal('Error','شما احراز هویت نشده اید','error');
                this.props.history.push('/');

            }else if (error.response.status===300) {
                 swal('Error','شما احراز هویت نشده اید','error');
                this.props.history.push('/');

                
            }
            else if (error.response.status===500) {
                this.props.history.replace('/admin');

                
            }

        })

    
    }
    render() { 
        return <Master {...this.props}/>;
    }
}
 
export default AdminRouter;