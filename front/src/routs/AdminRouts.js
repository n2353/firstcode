
import AboutUs from '../components/admin/about_us/AboutUs';
import Dashboard from '../components/admin/Dashboard';
import AdminPosts from '../components/admin/posts/AdminPosts';
import CreatePost from '../components/admin/posts/CreatePost';
import EditPost from '../components/admin/posts/EditPost';

const adminRouts=[
    {id:1,path:'/admin/about-us',name:'about_us',component:AboutUs,exact:true},
    {id:1,path:'/admin/create-post',name:'create_post',component:CreatePost,exact:true},
    {id:2,path:'/admin/posts',name:'posts',component:AdminPosts,exact:true},
    {id:3,path:"/admin/edit-post/:id",name:'edit_post',component:EditPost,exact:true},
    {id:4,path:'/admin',name:'admin',component:Dashboard,exact:false},
];
export default adminRouts;