import axios from 'axios';
import React from 'react';
import { Link } from 'react-router-dom';
import  swal  from 'sweetalert';
class AdminPosts extends React.Component {

    state={
        posts:[]

    }
    componentDidMount(){
        axios.get('sanctum/csrf-cookie').then(response => {
            axios.get(axios.defaults.baseURL+'/api/getPosts').then(res=>{
                if(res.data.status===200){
                    this.setState({...this.state,posts:res.data.posts});
                    console.log(this.state.posts);
                }
                else{

                }
            });
        });
    }
    deletePost=(e,id)=>{
        e.preventDefault();
        const originPosts=this.state.posts;
        const mainPosts=[];
        this.state.posts.forEach(post => {
            post.id!==id?mainPosts.push(post):console.log();

            
        });
        this.setState({...this.state,posts:mainPosts});
        axios.get('sanctum/csrf-cookie').then(response => {
            axios.delete(axios.defaults.baseURL+'/api/deletePost/'+id).then(res=>{
                if(res.data.status!==200){
                    swal('خطا','خطا در انجام عملیات','error');
                    this.setState({...this.state,posts:originPosts})
                }
                else{
                    swal('با موفقییت حذف شد','موفق','success');

                }
            });
        });
    }
    render() { 
        return (
            <div className="container pt-5" >
                <div className="card ">
                    <div className="card-header text-center">
                    <h4>نمایش بست ها</h4>
                        
                    </div>
                    <div style={{ fontSize:'22px' }} className="card-body">

                        {this.state.posts===[]?(
                            <h3>empty</h3>
                        ):(
                            <table className="table">
                            <thead className="thead-dark">
                                <tr>
                                    <th width='5%' scope="col">#</th>
                                    <th  width='10%'  scope="col">عنوان</th>
                                    <th width='20%'  scope="col">ادرس سایت</th>
                                    <th  width='40%'  scope="col">تصویر سایت</th>
                                    <th  width='25%'  scope="col">عملیات</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.posts.map(post=>{
                                    return (
                                        <tr key={post.id}>
                                            <th scope="row">{post.id}</th>
                                            <td>{post.title}</td>
                                            <td>{post.site}</td>
                                            <td><img width='35%' src={axios.defaults.baseURL+'/'+post.photo} alt="" /></td>
                                            <td><button style={{ padding:10,margin:5 }} onClick={(e)=>{this.deletePost(e,post.id)}} className="btn btn-danger" >حذف</button>
                                            <button onClick={(e)=>{e.preventDefault();this.props.history.replace({pathname:'edit-post/'+post.id,post_id:post.id})}} style={{ padding:10,margin:5 }} className="btn btn-warning">ویرایش</button></td>
                                        </tr>

                                    )
                                })}
                                
                                
                            </tbody>
                            </table>

                        )}
                    </div>
                </div>
            </div>
        );
    }
}
 
export default AdminPosts;