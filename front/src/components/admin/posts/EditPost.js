import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { changeInputs, handleErrors, handleFile, handleInput } from '../../../actions';
class EditPost extends React.Component {
    state={
        post:{}
    }
    onSubmit=(e)=>{
            e.preventDefault();
            const formData=new FormData();

            formData.append('photo',this.props.photo?this.props.photo:'');
            formData.append('title',this.props.title?this.props.title:'');
            formData.append('description',this.props.description?this.props.description:'');
            formData.append('site',this.props.site?this.props.site:'');
            console.log(formData);

        axios.get('sanctum/csrf-cookie').then(response => {
            axios.post(axios.defaults.baseURL+'/api/edit-post/'+this.state.post.id,formData).then(res=>{
                if(res.data.status===200){

                     swal('Success',res.data.message,'success');
                }
                else{
                    this.props.dispatch(handleErrors(res.data.errors));

                }
            });
        });

        }

        componentDidMount(){
           
            axios.get('sanctum/csrf-cookie').then(response => {
            axios.get(axios.defaults.baseURL+'/api/get-post/'+this.props.location.post_id).then(res=>{
                if(res.data.status===200){
                     this.setState({...this.state,post:res.data.post});
                     this.props.dispatch(changeInputs('title',this.state.post.title));
                     this.props.dispatch(changeInputs('site',this.state.post.site));
                     this.props.dispatch(changeInputs('description',this.state.post.description));
                    
                }
                else{
                    this.props.history.push('/admin/posts');

                }
            });
        });

        }
    render() { 


        
        return (
            <div className="container pt-5" >
            <div className="card ">
                <div className="card-header text-center">
                <h4>ویرایش بست</h4>
                    
                </div>
                <div style={{ fontSize:'22px' }} className="card-body">
                    <form method="post" onSubmit={this.onSubmit} style={{ direction:'rtl' }} enctype="multipart/form-data">
                    <div className="form-group p-2" >
                    <label  for="formFileLg" className="form-label">تصویر سایت:</label>
                    <input aria-label="Large" onChange={(e)=>{this.props.dispatch(handleFile(e))}} name='photo' className="form-control form-control-lg" id="formFileLg" type="file"/>
                    <span style={{ color:'red' }} >{this.props.errors.photo}</span>

                    <img src={axios.defaults.baseURL+'/'+this.state.post.photo} width={'30%'} alt="" />

                    </div>
                    <div className="form-group p-2">
                        <label  for="exampleInputEmail1">عنوان</label>
                        <input type="text" name="title" value={this.props.title} onChange={(e)=>{this.props.dispatch(handleInput(e))}} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="عنوان بست"/>
                        <span style={{ color:'red' }} >{this.props.errors.title}</span>
                        <small id="emailHelp" className="form-text text-muted"></small>
                    </div>
                    <div className="form-group">
                        <label  for="exampleInputEmail1">ادرس سایت</label>
                        
                    </div>
                    <div class="input-group mb-3 p-2">

                    
                        <input type="text" name="site" value={this.props.site} onChange={(e)=>{this.props.dispatch(handleInput(e))}} style={{ direction:'ltr' }} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="ادرس سایت"/>
                        
                        <div className="input-group-prepend">
                        
                        <span className="input-group-text" id="basic-addon1">//:https</span>
                        
                    </div>
                                            <span style={{ color:'red' }} >{this.props.errors.site}</span>

                    
                    </div>

                    <div className="form-group p-2">
                        <label  for="exampleInputEmail1">توضیحات</label>
                        <textarea name="description" value={this.props.description} onChange={(e)=>{this.props.dispatch(handleInput(e))}} className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="توضیحات بست"/>
                        <span style={{ color:'red' }} >{this.props.errors.description}</span>
                        <small id="emailHelp" className="form-text text-muted"></small>
                    </div>

                    <div className="form-group p-2 text-center">
                        <button type="submit" className="btn btn-info p-sm-4" style={{ fontSize:'25px' }}>تایید</button>
                    </div>
                    
                </form>
                </div>
                
                </div>
                
            </div>
            
        );
    }
}
 
export default connect((state)=>{return state}) (EditPost);
