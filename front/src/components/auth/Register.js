import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { handleErrors, handleInput } from '../../actions';


class Register extends React.Component {
    

    // handleInput=(e)=>{
    //     this.setState({
    //         ...this.state,
    //         [e.target.name]:e.target.value,
            
    //     });

    // }
    onSubmit=(e)=>{

        e.preventDefault();
        const data={
            name:this.props.name?this.props.name:'',
            email:this.props.email?this.props.email:'',
            password:this.props.password?this.props.password:'',
        }

        axios.get('sanctum/csrf-cookie').then(response => {
            axios.post(axios.defaults.baseURL+'/api/register',data).then(res=>{
                if(res.data.status===200){
                    localStorage.setItem('auth_token',res.data.token);
                    localStorage.setItem('auth_name',res.data.name);
                    swal('Success',res.data.message,'success');
                    this.props.history.push('/');
                }
                else{
                    this.props.dispatch(handleErrors(res.data.errors));
                }
            });
        });
        
    }
    render() { 
        return (
            <div>
                <div className="container py-5">
                    <div className="row justify-content-center">
                        <div className="col-md-6">
                            <div className="card">
                                <div className="card-header">
                                 <h4>فرم ثبت نام</h4>
                                </div>
                                <div className="card-body">
                                    <form onSubmit={this.onSubmit}>
                                        <div className="form-group mb-3">
                                        <label>نام ونام خانوادگی</label>
                                        <input name="name" onChange={(e)=>{this.props.dispatch(handleInput(e))}} className="form-control" type="text"  />
                                        <span style={{ color:'red' }} >{this.props.errors.name}</span>

                                        </div>
                                        <div className="form-group mb-3">
                                        <label>ایمیل</label>
                                        <input className="form-control" onChange={(e)=>{this.props.dispatch(handleInput(e))}}  name="email" type="text"  />
                                        <span style={{ color:'red' }}>{this.props.errors.email}</span>

                                        </div>
                                        <div className="form-group mb-3">
                                        <label>رمز عبور</label>
                                        <input className="form-control" onChange={(e)=>{this.props.dispatch(handleInput(e))}}  name="password" type="password"  />
                                        <span style={{ color:'red' }}> {this.props.errors.password}</span>
                                        </div>
                                       
                                        <div className="form-group mb-3">

                                            <button type="submit" className="btn btn-primary">ثبت نام</button>
                                        </div>
                                        

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>);
    }
}
 
export default connect((state)=>{return state}) (Register);
