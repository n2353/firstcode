import axios from 'axios';
import React from 'react';
import { connect } from 'react-redux';
import swal from 'sweetalert';
import { changeToken, handleErrors, handleInput } from '../../actions';


class Login extends React.Component {
    

    // handleInput=(e)=>{
    //     this.setState({
    //         ...this.state,
    //         [e.target.name]:e.target.value,
            
    //     });

    // }
    componentDidMount(){

        axios.interceptors.response.use((response)=>{
            return response
        },(error)=>{
            if(error.response.status===429){
                swal('Error','تعداد درخواست بیش از حد مجاز','error');
            }

        })
    }
    onSubmit=(e)=>{

        e.preventDefault();
        const data={
            email:this.props.email?this.props.email:'',
            password:this.props.password?this.props.password:'',
        }

        axios.get('sanctum/csrf-cookie').then(response => {
            axios.post(axios.defaults.baseURL+'/api/login',data).then(res=>{
                try {
                    if(res.data.status===200){
                    localStorage.setItem('auth_token',res.data.token);
                    localStorage.setItem('auth_name',res.data.name);
                    this.props.dispatch(changeToken(localStorage.getItem('auth_token')?localStorage.getItem('auth_token'):''));
                    swal('Success',res.data.message,'success');
                    this.props.history.push('/');
                    }
                    else if(res.data.status===420){
                        this.props.dispatch(handleErrors(res.data.errors));
                    }
                    else if(res.data.status===300){
                        swal('Error',res.data.message,'error');
                    }
                } catch (error) {
                    
                }
            });
        });
        
    }
    render() { 
        return (
            <div>
                <div className="container py-5">
                    <div className="row justify-content-center">
                        <div className="col-md-6">
                            <div className="card">
                                <div className="card-header">
                                 <h4>فرم ورود</h4>
                                </div>
                                <div className="card-body">
                                    <form onSubmit={this.onSubmit}>
                                        
                                        <div className="form-group mb-3">
                                        <label>ایمیل</label>
                                        <input className="form-control" onChange={(e)=>{this.props.dispatch(handleInput(e))}}  name="email" type="text"  />
                                        <span style={{ color:'red' }}>{this.props.errors.email}</span>

                                        </div>
                                        <div className="form-group mb-3">
                                        <label>رمز عبور</label>
                                        <input className="form-control" onChange={(e)=>{this.props.dispatch(handleInput(e))}}  name="password" type="password"  />
                                        <span style={{ color:'red' }}> {this.props.errors.password}</span>
                                        </div>
                                       
                                        <div className="form-group mb-3">

                                            <button type="submit" className="btn btn-primary">ورود</button>
                                        </div>
                                        

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>);
    }
}
 
export default connect((state)=>{return state}) (Login);
