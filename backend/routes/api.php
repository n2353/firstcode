<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Ui\AuthCommand;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register',[AuthController::class,'register']);
Route::middleware('throttle:3,5')->post('login',[AuthController::class,'login']);

Route::middleware(['auth:sanctum','check_admin'])->group(function(){
    Route::get('checkingAuthenticated',[AuthController::class,'checkAdmin']);
    Route::post('create-post',[PostController::class,'store']);
    Route::get('getPosts',[PostController::class,'getPosts']);
    Route::get('get-post/{id}',[PostController::class,'getPost']);
    Route::post('edit-post/{id}',[PostController::class,'editPost']);
    Route::delete('deletePost/{id}',[PostController::class,'deletePost']);

});


Route::middleware(['auth:sanctum'])->group(function(){
    Route::get('logout',[AuthController::class,'logout']);

});