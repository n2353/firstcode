<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    { if(auth('sanctum')->check())
        {
            if (auth('sanctum')->user()->tokenCan('server:admin')) {
                        return $next($request);
            }else{
                return response()->json(['status'=>320,'message'=>'You Dont Have Admin Permission'],320);
            }
        }
        else{
            return response()->json(['status'=>300,'message'=>'You Are not Authenticated'],300);

        }

    }
}