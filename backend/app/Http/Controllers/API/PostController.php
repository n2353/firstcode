<?php

namespace App\Http\Controllers\API;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    //
    public function store(Request $request)
    {

         $messages=[
            'required'=>'این فیلد اجباری است.',
            'image'=>'تصویر محتوا باید با فرمت jpg,jpeg, png باشد .,',
            'mimes'=>'تصویر محتوا باید با فرمت jpg,jpeg, png باشد .,',
            'max'=>'حجم فایل باید حد اکثر 2 مگابایت باشد'
        ];
        $validator=Validator::make($request->all(),[
            'title'=>'required|max:191',
            'photo'=>'image|mimes:jpg,png,jpeg|max:2048',
            'description'=>'required',
            'site'=>'required|max:191',
        ],$messages);
        if($validator->fails()){
            return response()->json([
                'status'=>420,
                'errors'=>$validator->messages()
            ]);
         }else{
             $post=new Post();
             $post->title=$request->title;
             $post->description=$request->description;
             $post->site=$request->site;
            $post->user_id=auth('sanctum')->id();
            if($request->hasFile('photo')){
                $file=$request->file('photo');
                $extension=$file->getClientOriginalExtension();
                $fileName=time().'.'.$extension;
                $file->move('uploads/posts/',$fileName);
                $post->photo='uploads/posts/'.$fileName;
            }
            $post->save();
            return response([
                'status'=>200,
                'message'=>'بست با موفقیت ذخیره شد '
            ]);
         }

    }

    public function getPosts()
    {
        $posts=Post::all();
        return response()->json([
           'status'=>200,
           'posts'=>$posts
        ]);

    }

    public function deletePost($id)
    {
        $post=Post::find($id);
        if ($post) {
            File::delete($post->photo);
            Post::destroy($id);
            return response()->json([
                'status'=>200
            ]);

        }else{
            return response()->json([
                'status'=>404
            ]);
        }

    }

    public function getPost($id)
    {
        $post=Post::find($id);
        if ($post)
        {
            return response()->json([
                'status'=>200,
                'post'=>$post
            ]);

        }else{
            return response()->json([
                'status'=>404
            ]);
        }


    }

    public function editPost($id,Request $request)
    {
        $messages=[
            'required'=>'این فیلد اجباری است.',
            'image'=>'تصویر محتوا باید با فرمت jpg,jpeg, png باشد .,',
            'mimes'=>'تصویر محتوا باید با فرمت jpg,jpeg, png باشد .,',
            'max'=>'حجم فایل باید حد اکثر 2 مگابایت باشد'
        ];
        $validator=Validator::make($request->all(),[
            'title'=>'required|max:191',
            'photo'=>'image|mimes:jpg,png,jpeg|max:2048',
            'description'=>'required',
            'site'=>'required|max:191',
        ],$messages);
        if($validator->fails()){
            return response()->json([
                'status'=>420,
                'errors'=>$validator->messages()
            ]);
         }else{
             $post=Post::find($id);
             $post->title=$request->title;
             $post->description=$request->description;
             $post->site=$request->site;
            $post->user_id=auth('sanctum')->id();
            if($request->hasFile('photo')){
                File::delete($post->photo);
                $file=$request->file('photo');
                $extension=$file->getClientOriginalExtension();
                $fileName=time().'.'.$extension;
                $file->move('uploads/posts/',$fileName);
                $post->photo='uploads/posts/'.$fileName;
            }
            $post->save();
            return response([
                'status'=>200,
                'message'=>'بست با موفقیت ویرایش شد '
            ]);
         }

    }
}