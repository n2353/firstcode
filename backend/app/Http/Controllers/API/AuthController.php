<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Testing\Fluent\Concerns\Has;

class AuthController extends Controller
{
    //
    public function register(Request $request)
    {

        $messages=[
            'required'=>'این فیلد اجباری است.',
            'email'=>'ایمیل نامعتبر.',
            'min'=>'رمز عبور باید بیش از 8 کارامتر باشد',
        ];
        $validator=Validator::make($request->all(),[
            'name'=>'required|max:191',
            'email'=>'required|email',
            'password'=>'required|max:191|min:8',
        ],$messages);

        if($validator->fails()){
            return response()->json(['status'=>420,'errors'=>$validator->messages()]);
        }
        else{
            $user=new User();
            $user->name=$request->name;
            $user->email=$request->email;
            $user->password=Hash::make($request->password);
            $user->save();
            $token=$user->createToken($user->email.'_token',[""])->plainTextToken;

            return response()->json([
                'status'=>200,
                'name'=>$user->name,
                'token'=>$token,
                'message'=>'ثبت نام با موفقییت انجام شد'

            ]);
        }

    }
    public function login(Request $request)
    {
        $messages=[
            'required'=>'این فیلد اجباری است.',
            'email'=>'ایمیل نامعتبر.',
            'min'=>'رمز عبور باید بیش از 8 کارامتر باشد',
        ];
        $validator=Validator::make($request->all(),[
            'email'=>'required|email',
            'password'=>'required|max:191|min:8',
        ],$messages);

        if($validator->fails()){
            return response()->json(['status'=>420,'errors'=>$validator->messages()]);
        }
        else{
            $user=User::where('email',$request->email)->first();
            if($user && Hash::check($request->password,$user->password)){
                if($user->role_id=='1'){
                    $token=$user->createToken($user->email.'_token',["server:admin"])->plainTextToken;

                }else{
                    $token=$user->createToken($user->email.'_token',[""])->plainTextToken;
                }
                return response()->json([
                'status'=>200,
                'name'=>$user->name,
                'token'=>$token,
                'message'=>'ورود با موفقییت انجام شد'

            ]);

            }else{
                return response()->json([
                    'status'=>300,
                    'message'=>'ایمیل یا رمز عبور اشتباه است'
                ]);
            }

        }

    }
    public function checkAdmin()
    {
        return response()->json(['status'=>200,'message'=>'success']);
    }


    public function logout()
    {
        if(auth('sanctum')->check()){
            auth('sanctum')->user()->tokens()->delete();
            return response()->json([
                'status'=>200,'message'=>'logged out'
            ]);
        }
    }
}